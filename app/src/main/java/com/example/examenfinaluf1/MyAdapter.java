package com.example.examenfinaluf1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private ArrayList<Guide> guides;
    private Fragment context;
    private DetailsFragment detailsFragment;

    public MyAdapter(ArrayList<Guide> guides, Fragment context) {
        this.guides = guides;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context.getContext());
        View view = inflater.inflate(R.layout.my_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(guides.get(position).getGuideImage())            //SE QUE NO CAL PICASSO, PERO EM FORMATEJA AUTOMATICAMENT LES IMATGES I ES COMODE
                .fit()
                .centerCrop()
                .into(holder.imageRow);
        holder.guideRow.setText(guides.get(position).getGuideName());
        holder.cityRow.setText(guides.get(position).getGuideCity());
        holder.priceRow.setText(guides.get(position).getGuidePrice());

        holder.relLayout.setOnClickListener(v -> {
            detailsFragment = DetailsFragment.newInstance(guides.get(holder.getAdapterPosition()).getGuideName(),
                    guides.get(holder.getAdapterPosition()).getGuideCity(), guides.get(holder.getAdapterPosition()).getGuidePrice(),
                    guides.get(holder.getAdapterPosition()).getGuideDesc(), guides.get(holder.getAdapterPosition()).getGuideImages(),
                    guides.get(holder.getAdapterPosition()).getGuideImage());
            FragmentManager fragmentManager= ((FragmentActivity)context.getContext()).getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.main_frame,detailsFragment);
            fragmentTransaction.commit();
        });
    }

    @Override
    public int getItemCount() {
        return guides.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageRow;
        private TextView guideRow;
        private TextView cityRow;
        private TextView priceRow;

        private RelativeLayout relLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageRow = itemView.findViewById(R.id.imageRow);
            guideRow = itemView.findViewById(R.id.guideRow);
            cityRow = itemView.findViewById(R.id.cityRow);
            priceRow = itemView.findViewById(R.id.priceRow);

            relLayout = itemView.findViewById(R.id.relLayout);
        }
    }
}
