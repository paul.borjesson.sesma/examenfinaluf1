package com.example.examenfinaluf1;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailsFragment extends Fragment {

    private ImageView imageDetail;
    private TextView titleDetail;
    private TextView cityDetail;
    private TextView priceDetail;
    private TextView descDetail;

    public DetailsFragment(ImageView imageDetail, TextView titleDetail, TextView cityDetail, TextView priceDetail, TextView descDetail, ArrayList<Integer> images) {
        this.imageDetail = imageDetail;
        this.titleDetail = titleDetail;
        this.cityDetail = cityDetail;
        this.priceDetail = priceDetail;
        this.descDetail = descDetail;
    }

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "name";
    private static final String ARG_PARAM2 = "city";
    private static final String ARG_PARAM3 = "price";
    private static final String ARG_PARAM4 = "desc";
    private static final String ARG_PARAM5 = "images";
    private static final String ARG_PARAM6 = "image";

    // TODO: Rename and change types of parameters
    private String paramGuidename;
    private String paramGuidecity;
    private String paramGuideprice;
    private String paramGuidedesc;
    private ArrayList<Integer> paramImages;
    private int paramImage;

    public DetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailsFragment newInstance(String param1, String param2) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static DetailsFragment newInstance(String guideName, String guideCity, String guidePrice, String guideDesc, ArrayList<Integer> guideImages, int guideImage) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, guideName);
        args.putString(ARG_PARAM2, guideCity);
        args.putString(ARG_PARAM3, guidePrice);
        args.putString(ARG_PARAM4, guideDesc);
        args.putIntegerArrayList(ARG_PARAM5, guideImages);
        args.putInt(ARG_PARAM6, guideImage);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        TextView titleDetail = view.findViewById(R.id.titleDetail);
        TextView cityDetail = view.findViewById(R.id.cityDetail);
        TextView priceDetail = view.findViewById(R.id.priceDetail);
        TextView descDetail = view.findViewById(R.id.descDetail);
        ImageView imageDetail = view.findViewById(R.id.imageDetail);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerDetail);

        if (getArguments() != null) {
            paramGuidename = getArguments().getString(ARG_PARAM1);
            paramGuidecity = getArguments().getString(ARG_PARAM2);
            paramGuideprice = getArguments().getString(ARG_PARAM3);
            paramGuidedesc = getArguments().getString(ARG_PARAM4);
            paramImages = getArguments().getIntegerArrayList(ARG_PARAM5);
            paramImage = getArguments().getInt(ARG_PARAM6);

            titleDetail.setText(paramGuidename);
            cityDetail.setText(paramGuidecity);
            priceDetail.setText(paramGuideprice);
            descDetail.setText(paramGuidedesc);
            Picasso.get().load(paramImage)              //SE QUE NO CAL PICASSO PERO EM FORMATEJA BE LES FOTOS AUTOMATICAMENT, ES COMODE
                    .fit()
                    .centerCrop()
                    .into(imageDetail);

            MyAdapter2 myAdapter2 = new MyAdapter2(paramImages, DetailsFragment.this);
            recyclerView.setAdapter(myAdapter2);
            recyclerView.setLayoutManager(new LinearLayoutManager(container.getContext()));
        }

        return view;
    }
}